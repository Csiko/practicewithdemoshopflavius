package org.fasttrackit;

public class DemoShopMain {
    public static void main(String[] args) {

        System.out.println("- = Demo Shop = - ");
        System.out.println("1. User can login with valid credentials.");
        Page page = new Page();
        page.openHomePage();
        Header header = new Header();
        header.clickOnTheLoginButton();
        ModalDialog modal = new ModalDialog();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        String greatingsMessage = header.getGreetingMessage();
        boolean isLogged = greatingsMessage.contains("dino");
        // aici urmeaza validarea
        System.out.println("Hi Dino is displayed in the header: " + isLogged);
        System.out.println("Greatings msg is: " + greatingsMessage);
        //2
        System.out.println("----------------------------------------");
        System.out.println("2. User can add product to cart from product cards");
        page.openHomePage();
        ProductCards cards = new ProductCards();
        //Product awesomeGraniteChips = cards.getProductByName("Awesome Granite Chips");
        //System.out.println("Product is: " + awesomeGraniteChips.getTitle());
        //awesomeGraniteChips.clickOnTheProductCartIcon();
        //3
        System.out.println("-----------------------------------------");
        System.out.println("3. User can navigate to Home page from Wishlist page.");
        page.openHomePage();
        header.clickOnTheWishlistIcon();
        String pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Wishlist page. Title is:" + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Home page. Title is:" + pageTitle);
        //4
        System.out.println("-----------------------------------------");
        System.out.println("4. User can navigate to Home page from Cart page.");
        page.openHomePage();
        header.clickOnTheCartIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Cart page. Title is:" + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Home page. Title is:" + pageTitle);

    }
}

// test dimensiune diferita la icoane exemplu login 16x16 wish 16x14

// - User can able to navigate to Home Page from Cart Page.
// 1. Open Home Page.
// 2. Click on the "Cart" icon.
// 3. click on the "Shopping Bag"
// Expected results: Home Page is loaded.

// - User can able to navigate to Home Page from Wishlist Page.
// 1. Open Home Page.
// 2. Click on the "favorites" icon.
// 3. click on the "Shopping Bag"
// Expected results: Home Page is loaded.


// ------
// - test 2 - User can add product to cart from product cards
// 1. Open Home page.
// 2 . Click on the "Awesome Granite Chips" cart icon.
// Expected results: Mini Cart Icon shows 1 product in cart.


// -test 1- User can login with valid credentials.
// 1. Open Home page.
// 2. Click on the Login button.
// 3. Clik on Username field.
// 4. Type in : Dino
// 5. Click on Password field.
// 6. Type in : chocohoo
// 7. Click on the Login button.

// ShortCut
// psvm - PublicStaticVoid
// sout - SystemOutPrintLine
// CTRL + / - Coment out code  (comenteaza linia de cod fara a evea efect in run)
// CTRL + ALT + L - Format Code (rearanjeaza liniile de cod)
// CTRL + D - duplicate line
// ; - CloseLineCode
// CTRL+ALT+V - Extract Variable from Selection
// CTRL + Alt + L - formating page
// CTRL + Alt + M - creating extras Method
// SHIFT +  F6 - Rename Variable
// variable.sout - print out the variable
// CTRL + z - Undo
// ALT + ENTER ! - oriunde se poate 8)
// CTRL + Shift + Z - Redo
// CTRL + Shift + UP - duci linia in sus cu text cu tot
// CTRL + Shift + down - duci linia in jos cu text cu tot