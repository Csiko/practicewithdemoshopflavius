package org.fasttrackit;


import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {
    private final SelenideElement loginButton = $(".navbar .fa-sign-in-alt");

    private final SelenideElement greatingElement = $(".navbar-text span span");

    private final SelenideElement whishlistButton = $(".navbar .fa-heart");

    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
    private final ElementsCollection shoppingCartBadges = $$(".shopping_cart_badge");

    private final SelenideElement homePageButton = $("[data-icon=shopping-bag]");

    public void clickOnTheLoginButton() {
        loginButton.click();
        System.out.println("Clicking on the Login button.");
    }

    public String getGreetingMessage() {
        return greatingElement.text();
    }

    public void clickOnTheWishlistIcon() {
        System.out.println("Clicking On the Wishlist button.");
        whishlistButton.click();
    }

    public void clickOnTheShoppingBagIcon() {
        System.out.println("Click on the Shopping Bag icon.");
        homePageButton.click();
    }

    public void clickOnTheCartIcon() {
        System.out.println("Click on the Cart icon.");
        cartIcon.click();
    }
    public String getShoppingCartBadgeValue() {
        return this.shoppingCartBadge.text();
    }

    public boolean isShoppingBadgeVisible() {
        return !this.shoppingCartBadges.isEmpty();
    }
}
